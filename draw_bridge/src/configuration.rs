/// # Configuration.
///
/// So. A huge amount of this is undocumented. I pulled everything I could find
/// out of a copy of the InkScape drivers, which have a lot of comments, but not
/// a lot of the right explanation. Which is... great.
///
/// As such: I'm skipping serialization on a *lot* of the fields in here until
/// I'm actually confident in what they do and whether they're needed. I'm using
/// precious few of them. I'll enable configs as they become transparently useful.
use {
    crate::plotter::RegisteredPlotters,
    serde::{Deserialize, Serialize},
    std::fmt,
};

// So, this is a bit of a puzzle.
//
// There are a *lot* of things you can know about a Plotter: maximum X and Y
// travel in inches; steps per inch at different "resolution factors"; the
// Nickname of the plotter. The list kinda goes on on and on.
//
// I can either:
// - Provide a strongly typed, but quite possibly very long, config object
// - Just let users pass strings and values, and do a bunch of munging.
//
// I honestly hate both options.
//
// Okay: most configs are get-able, but only some are set-able. That means two
// mapping types, which is... fine.
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum GettableConfig {
    /// X (Long) Carriage travel in inches
    XTravel,
    /// Y (Short)Carriage travel in inches
    YTravel,
    /// Motor resolution calculation factor, in steps per inch; used in conversions.
    /// Resolution is NativeResFactor * sqrt(2) steps per inch in Low Resolution  (Approx 1437 steps per inch),
    ///     and 2 * NativeResFactor * sqrt(2) steps per inch in High Resolution (Appxox 2874 steps per inch)
    #[serde(skip_serializing)]
    NativeResolutionFactor,
    /// Maximum allowed motor step rate, in steps per millisecond. 25kHz is the
    /// absolute maximum EBB step rate.
    #[serde(skip_serializing)]
    MaxStepRate,
    /// Maximum X/Y speed allowed in inches per second.
    #[serde(skip_serializing)]
    SpeedLimit,
    /// Maximum distance covered by one step.
    /// Low res: ~1/(`native_resolution_factor` * sqrt(2))
    /// High res: ~1/(2 * `native_resolution_factor` * sqrt(2))
    #[serde(skip_serializing)]
    MaxStepDistance,
    /// A factor applied to pen-down speed when in "constant speed mode", whatever that is.
    #[serde(skip_serializing)]
    ConstantSpeedFactor,
    #[serde(skip_serializing)]
    AccelerationRate,
    /// Interval, in seconds, of when to update the motors.
    #[serde(skip_serializing)]
    TimeSlice,
    /// Okay this one is sincerely strange. According to the source comments:
    /// Set a tolerance value, to avoid error messages if a "zero" position
    /// rounds to -0.0000010 or something. Why one wouldn't just fix rounding I
    /// have *no idea*.
    #[serde(skip_serializing)]
    BoundsTolerance,
    /// Allow sufficiently short pen-up moves to be substituted with a pen-down move
    #[serde(skip_serializing)]
    MinGap,
    /// Servo motion limits, in units of 1/12MHz
    #[serde(skip_serializing)]
    ServoMax,
    #[serde(skip_serializing)]
    ServoMin,
    /// Whether or not to skip the voltage check. This will always be false, but you can check if you like.
    #[serde(skip_serializing)]
    SkipVoltageCheck,
}

impl fmt::Display for GettableConfig {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            GettableConfig::XTravel => "x_travel",
            GettableConfig::YTravel => "y_travel",
            GettableConfig::NativeResolutionFactor => "native_resolution_factor",
            GettableConfig::MaxStepRate => "max_step_rate",
            GettableConfig::SpeedLimit => "speed_limit",
            GettableConfig::MaxStepDistance => "max_step_distance",
            GettableConfig::ConstantSpeedFactor => "constant_speed_factor",
            GettableConfig::AccelerationRate => "acceleration_rate",
            GettableConfig::TimeSlice => "time_slice",
            GettableConfig::BoundsTolerance => "bounds_tolerance",
            GettableConfig::MinGap => "min_gap",
            GettableConfig::ServoMax => "servo_max",
            GettableConfig::ServoMin => "servo_min",
            GettableConfig::SkipVoltageCheck => "skip_voltage_check",
        };
        write!(f, "{}", s)
    }
}

/// Some configs can be set via the API. Note that the defaults are also the
/// maximum allowed values; overrides can be less, but never more. For instance:
/// the default XTravel is already the maximum that that plotter can perform. It
/// can be artificially restricted, but never artificially expanded.
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum SettableConfigs {
    XTravel(f64),
    YTravel(f64),
}

pub enum ConfigurationError {
    InvalidValue(String),
    InvalidParams(String),
}

// TODO[gastove|2020-06-18] derive Default
#[derive(Debug, Default, Clone, Copy, Serialize, PartialEq, Deserialize)]
pub struct AccelerationRate {
    pub pen_up: f64,
    pub pen_down: f64,
}

impl AccelerationRate {
    fn new(up: f64, down: f64) -> Self {
        Self {
            pen_up: up,
            pen_down: down,
        }
    }
}

impl fmt::Display for AccelerationRate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "AccelerationRate<PenUp:{}, PenDown:{}>",
            self.pen_up, self.pen_down
        )
    }
}

#[derive(Debug, Default, Clone, Copy, Serialize, PartialEq, Deserialize)]
pub struct ResolutionMode<T>
where
    T: fmt::Display + Default,
{
    pub high: T,
    pub low: T,
}

impl<T> ResolutionMode<T>
where
    T: fmt::Display + Default,
{
    fn new(h: T, l: T) -> Self {
        Self { high: h, low: l }
    }
}

impl<T> fmt::Display for ResolutionMode<T>
where
    T: fmt::Display + Default,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "high:{}; low:{}", self.high, self.low)
    }
}

// TODO: Get *all* of this fully converted to metric

/// Base configuration struct for a given plotter. Note: I'm currently coding in
/// support for resolution modes (high, low) and speed modes (constant, "the
/// other one"), but these things are mostly totally undocumented. This is not
/// quite cargo-culting, but it's not *not* cargo-culting either.
///
/// Here in version < 1, this will be a pretty straight translation of all the
/// constants the Inkscape python driver felt were important to hard-code; doc
/// strings are lifted straight from the corresponding comments in that code.
/// This will get updated over time as I figure out what, specifically, I need
/// to *do* with all this stuff.
#[derive(Debug, Copy, Clone, Serialize, PartialEq, Deserialize)]
pub struct PlotterConfig {
    /// X (Long) Carriage travel in inches -> 430mm
    pub x_travel: f64,
    /// Y (Short)Carriage travel in inches -> 297mm
    pub y_travel: f64,
    /// Motor resolution calculation factor, in steps per millimeter.
    #[serde(skip)]
    pub native_resolution_factor: i32,
    /// Maximum allowed motor step rate, in steps per millisecond. 25kHz is the
    /// absolute maximum EBB step rate.
    #[serde(skip)]
    pub max_step_rate: f64,
    /// Maximum X/Y speed allowed in inches per second.
    #[serde(skip)]
    pub speed_limit: ResolutionMode<f64>,
    /// Maximum distance covered by one step.
    /// Low res: ~1/(`native_resolution_factor` * sqrt(2))
    /// High res: ~1/(2 * `native_resolution_factor` * sqrt(2))
    #[serde(skip)]
    pub max_step_distance: ResolutionMode<f64>,
    /// A factor applied to pen-down speed when in "constant speed mode", whatever that is.
    #[serde(skip)]
    pub constant_speed_factor: ResolutionMode<f64>,
    #[serde(skip)]
    pub acceleration_rate: AccelerationRate,
    /// Interval, in seconds, of when to update the motors.
    // TODO[gastove|2021-01-04] Consider using an actual Duration.
    #[serde(skip)]
    pub time_slice: f64,
    /// Okay this one is sincerely strange. According to the source comments:
    /// Set a tolerance value, to avoid error messages if a "zero" position
    /// rounds to -0.0000010 or something. Why one wouldn't just fix rounding I
    /// have *no idea*.
    #[serde(skip)]
    pub bounds_tolerance: f64,
    /// Allow sufficiently short pen-up moves to be substituted with a pen-down move
    #[serde(skip)]
    pub min_gap: f64,
    /// Servo motion limits, in units of 1/12MHz
    #[serde(skip)]
    pub servo_max: i64,
    #[serde(skip)]
    pub servo_min: i64,
    #[serde(skip)]
    pub skip_voltage_check: bool,
}

impl PlotterConfig {
    pub fn get_val(&self, key: &GettableConfig) -> String {
        match key {
            GettableConfig::XTravel => self.x_travel.to_string(),
            GettableConfig::YTravel => self.y_travel.to_string(),
            GettableConfig::NativeResolutionFactor => self.native_resolution_factor.to_string(),
            GettableConfig::MaxStepRate => self.max_step_rate.to_string(),
            GettableConfig::SpeedLimit => {
                serde_json::to_string(&self.speed_limit).expect("Failed to serialize")
            }
            GettableConfig::MaxStepDistance => {
                serde_json::to_string(&self.max_step_distance).expect("Failed to serialize")
            }
            GettableConfig::ConstantSpeedFactor => {
                serde_json::to_string(&self.constant_speed_factor).expect("Failed to serialize")
            }
            GettableConfig::AccelerationRate => {
                serde_json::to_string(&self.acceleration_rate).expect("Failed to serialize")
            }
            GettableConfig::TimeSlice => self.time_slice.to_string(),
            GettableConfig::BoundsTolerance => self.bounds_tolerance.to_string(),
            GettableConfig::MinGap => self.min_gap.to_string(),
            GettableConfig::ServoMax => self.servo_max.to_string(),
            GettableConfig::ServoMin => self.servo_min.to_string(),
            GettableConfig::SkipVoltageCheck => self.skip_voltage_check.to_string(),
        }
    }

    // TODO[gastove|2021-01-04] Somewhere in here, validation occurs.
    pub fn update(&self, key: &SettableConfigs) -> PlotterConfig {
        let mut updater = self.clone();

        match key {
            SettableConfigs::XTravel(x) => updater.x_travel = *x,
            SettableConfigs::YTravel(y) => updater.y_travel = *y,
        }

        updater
    }
}
// TODO[gastove|2021-01-04] Make this an associated method on PlotterConfig
pub fn load_config_for_plotter(plotter: &RegisteredPlotters) -> PlotterConfig {
    let axidraw_v3 = PlotterConfig {
        x_travel: 11.81, // 300mm
        y_travel: 8.58,  // 218mm
        native_resolution_factor: 1016,
        max_step_rate: 24.95,
        speed_limit: ResolutionMode::new(8.6979, 12.0),
        max_step_distance: ResolutionMode::new(0.000_348, 0.000_696),
        constant_speed_factor: ResolutionMode::new(0.4, 0.5),
        acceleration_rate: AccelerationRate::new(60.0, 40.0),
        time_slice: 0.025,
        bounds_tolerance: 0.001,
        min_gap: 0.008,
        servo_max: 27831,
        servo_min: 9855,
        skip_voltage_check: false,
    };

    let axidraw_v3_a3 = PlotterConfig {
        x_travel: 430.0,                // 16.93 in, 430mm; about 24328.41 steps
        y_travel: 297.0,                // 11.69, 297mm about 16798.53 steps
        native_resolution_factor: 1016, // Remember we are typiclly in Hi Res Mode, so double this
        max_step_rate: 24.95,
        speed_limit: ResolutionMode::new(8.6979, 12.0),
        max_step_distance: ResolutionMode::new(0.000_348, 0.000_696),
        constant_speed_factor: ResolutionMode::new(0.4, 0.5),
        acceleration_rate: AccelerationRate::new(60.0, 40.0),
        time_slice: 0.025,
        bounds_tolerance: 0.001,
        min_gap: 0.008,
        servo_max: 27831,
        servo_min: 9855,
        skip_voltage_check: false,
    };

    let axidraw_v3_xlx = PlotterConfig {
        x_travel: 23.42, // 595mm
        y_travel: 8.58,  // 218mm
        native_resolution_factor: 1016,
        max_step_rate: 24.95,
        speed_limit: ResolutionMode::new(8.6979, 12.0),
        max_step_distance: ResolutionMode::new(0.000_348, 0.000_696),
        constant_speed_factor: ResolutionMode::new(0.4, 0.5),
        acceleration_rate: AccelerationRate::new(60.0, 40.0),
        time_slice: 0.025,
        bounds_tolerance: 0.001,
        min_gap: 0.008,
        servo_max: 27831,
        servo_min: 9855,
        skip_voltage_check: false,
    };

    match plotter {
        RegisteredPlotters::AxiDrawV3 => axidraw_v3,
        RegisteredPlotters::AxiDrawV3A3 => axidraw_v3_a3,
        RegisteredPlotters::AxiDrawV3LXL => axidraw_v3_xlx,
    }
}
