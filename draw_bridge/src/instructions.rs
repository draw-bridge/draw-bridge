//! Instructions
//!
//! An instruction maps 1:1 with commands supported by the
//! [EiBotBoard](https://evil-mad.github.io/EggBot/ebb.html). Instructions are
//! modeled as an enum, `EBBInstruction`, which provides a common interface for
//! e.g. getting the bytes of an instruction. Every instruction also maps 1:1
//! with an expected type of response, and the enum knows how to parse those
//! responses as well.

// TODO[gastove|2020-06-20] Clarify that this module does _basically_ no error
// handling. This is a bit weird, so deserves clear docs.

use {
    regex::Regex,
    std::{
        convert::{From, TryFrom},
        fmt,
        time::Duration,
    },
};

// Regexen for parsing various plotter responses.
lazy_static! {
    static ref VERSION_RE: Regex =
        Regex::new(r"^EBBv13_and_above EB Firmware Version (\d+)\.(\d+)(?:\.(\d+))?$").unwrap();
    static ref MOTOR_STATUS_RE: Regex = Regex::new(r"^QM,(\d+),(\d+),(\d+),(\d+)$").unwrap();
}

/// The plotter always responds with at least one String; frequently, it
/// responds with more than one.
type PlotterResponse = Vec<String>;

/// EBB responses become a little idiosyncratic in the context of Rust Results,
/// as the generic "all is well" return value is the string "OK". We model this
/// as a Result all the same, as it's the most idiomatic. To avoid the very
/// strange feeling pattern of `Ok(EBBResponse::Ok)`, we model the EBB "Ok"
/// response as `Unit`, on the theory that "Ok" amounts to, "it worked and I
/// have nothing to say about it".
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum EBBResponse {
    #[serde(rename = "ok")]
    Unit,
    #[serde(rename = "firmware_version")]
    Version {
        major: u8,
        minor: u8,
        patch: u8,
    },
    Nickname {
        nickname: String,
    },
    Steps {
        x_pos: i32,
        y_pos: i32,
    },
    MotorStatus {
        executing: bool,
        x_motor_moving: bool,
        y_motor_moving: bool,
        motion_queue_empty: bool,
    },
    PenPosition(PenPosition),
    PenState {
        position: PenPosition,
        x: i32,
        y: i32,
    },
    Multiple(Vec<EBBResponse>),
}

impl fmt::Display for EBBResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EBBResponse::MotorStatus {
                executing: exec_ing,
                x_motor_moving: x_moving,
                y_motor_moving: y_moving,
                motion_queue_empty,
            } => write!(
                f,
                "Executing: {}; X Moving: {}; Y Moving: {}; Queue Empty: {}",
                exec_ing, x_moving, y_moving, motion_queue_empty
            ),
            EBBResponse::Nickname { nickname } => write!(f, "{}", nickname),
            EBBResponse::PenPosition(pos) => write!(f, "Pen is currently {}", pos),
            EBBResponse::Steps { x_pos, y_pos } => {
                write!(f, "Motor has X position {}, Y position {}", x_pos, y_pos)
            }
            EBBResponse::Unit => write!(f, "Ok"),
            EBBResponse::Version {
                major,
                minor,
                patch,
            } => write!(f, "Firmware is at version: {}.{}.{}", major, minor, patch),
            EBBResponse::PenState { position, x, y } => write!(
                f,
                "Pen is {} at X position {}, Y position {}",
                position, x, y
            ),
            EBBResponse::Multiple(_) => unimplemented!(),
        }
    }
}

// TODO[gastove|2020-06-20] Very broadly, we have no idea how an EBB instruction
// can both return a result *and* fail. So, for now, having String as an error
// type is about as descriptive as we can get.
pub type Result = std::result::Result<EBBResponse, String>;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum PenPosition {
    Up,
    Down,
}

impl fmt::Display for PenPosition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PenPosition::Up => write!(f, "Up"),
            PenPosition::Down => write!(f, "Down"),
        }
    }
}

impl TryFrom<&str> for PenPosition {
    type Error = String;

    fn try_from(s: &str) -> std::result::Result<PenPosition, String> {
        match s {
            "0" => Ok(PenPosition::Down),
            "1" => Ok(PenPosition::Up),
            _ => {
                let msg = format!("Invalid pen position: {}", s);
                Err(msg)
            }
        }
    }
}

/// EBBInstruction describes every EBB command `draw-bridge` currently knows how
/// to handle.
pub enum EBBInstruction {
    HomeMove,
    MixedGeometryMove { x: i32, y: i32, duration: Duration },
    QueryMotorStatus,
    QueryNickname,
    QueryPen,
    QueryCurrentPenPosition,
    SetNickname(String),
    SetPen(PenPosition),
    TogglePen,
    Version,
}

impl EBBInstruction {
    pub fn as_bytes(&self) -> Vec<u8> {
        let cmd_str = match self {
            EBBInstruction::HomeMove => home_move(),
            EBBInstruction::MixedGeometryMove { x, y, duration } => move_pen(*x, *y, *duration),
            EBBInstruction::QueryMotorStatus => query_motors(),
            EBBInstruction::QueryNickname => query_nickname(),
            EBBInstruction::QueryPen => query_pen(),
            EBBInstruction::QueryCurrentPenPosition => query_steps(),
            EBBInstruction::SetNickname(n) => set_nickname(n.to_owned()),
            EBBInstruction::SetPen(set_to) => set_pen(set_to),
            EBBInstruction::TogglePen => toggle_pen(),
            EBBInstruction::Version => version(),
        };

        cmd_str.into_bytes()
    }

    pub fn parse_response(&self, resp: PlotterResponse) -> Result {
        match self {
            EBBInstruction::HomeMove => parse_ok(resp),
            EBBInstruction::MixedGeometryMove { .. } => parse_ok(resp),
            EBBInstruction::QueryMotorStatus => parse_query_motor_response(resp),
            EBBInstruction::QueryPen => parse_query_pen_response(resp),
            EBBInstruction::QueryCurrentPenPosition => parse_query_steps_response(resp),
            EBBInstruction::SetPen(_) => parse_ok(resp),
            EBBInstruction::TogglePen => parse_ok(resp),
            EBBInstruction::Version => parse_version_response(resp),
            EBBInstruction::QueryNickname => parse_query_nickname_response(resp),
            EBBInstruction::SetNickname(n) => parse_ok(resp),
        }
    }

    /// The plotter responds to an instruction with one-or-more responses; we
    /// need to know how many responsese to read. This allows an instruction to
    /// override the default as needed.
    pub fn responses(&self) -> usize {
        match self {
            Self::QueryPen => 2,
            Self::QueryNickname => 2,
            _ => 1,
        }
    }
}

fn to_err_string(not_ok: PlotterResponse) -> String {
    not_ok
        .iter()
        .cloned()
        .filter(|s| !s.is_empty())
        .collect::<Vec<String>>()
        .join(",")
}

pub fn parse_ok(maybe_ok: PlotterResponse) -> Result {
    if maybe_ok.len() == 1 && maybe_ok[0] == "OK" {
        Ok(EBBResponse::Unit)
    } else {
        Err(to_err_string(maybe_ok))
    }
}

/// NOTE: We use `XM` (that's "mixed-axis geometries move"), *not* `SM`,
/// or "stepper move". Stepper move _moves both motors in sync_. XM does
/// the math to only move each motor the correct number of steps.
fn move_pen(x: i32, y: i32, duration: Duration) -> String {
    // TODO[gastove|2020-12-31] I'm going to have to actually do some math here
    // to try and compute a real duration. Turns out, the InkScape drivers have
    // like 600 lines of math, all on the topic of figuring out how fast each
    // move should go.
    //
    // *Oye*.
    //
    // Probably what I want is an API-level command that breaks motion in to
    // segments, then calls in to this function which will *require* a duration.

    format!("XM,{},{},{}\r", duration.as_millis(), x, y)
}

fn query_motors() -> String {
    String::from("QM\r")
}

pub fn version() -> String {
    String::from("V\r")
}

#[derive(Debug, Eq, PartialEq)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
}

fn match_to_u8(m: Option<regex::Match>) -> u8 {
    match m {
        Some(res) => {
            let parsed = res.as_str().parse();
            parsed.expect("Failed to parse string")
        }
        None => 0,
    }
}

fn match_to_i32(m: Option<regex::Match>) -> i32 {
    match m {
        Some(res) => {
            let parsed = res.as_str().parse();
            parsed.expect("Failed to parse string")
        }
        None => 0,
    }
}

fn match_to_bool(m: Option<regex::Match>) -> bool {
    match m {
        Some(res) => "0" == res.as_str(),
        None => false,
    }
}

pub struct MotorStatus {
    pub command_executing: bool,
    pub x_moving: bool,
    pub y_moving: bool,
    pub motion_queue_empty: bool,
}

pub fn parse_query_motor_response(resp: PlotterResponse) -> Result {
    if resp.len() == 1 {
        MOTOR_STATUS_RE.captures(&resp[0]).map_or(
            Err(format!("Couldn't parse EBB response: {}", resp.join(","))),
            |cap| {
                let exec_ing = match_to_bool(cap.get(1));
                let x_moving = match_to_bool(cap.get(2));
                let y_moving = match_to_bool(cap.get(3));
                let motion_queue_empty = match_to_bool(cap.get(4));

                Ok(EBBResponse::MotorStatus {
                    executing: exec_ing,
                    x_motor_moving: x_moving,
                    y_motor_moving: y_moving,
                    motion_queue_empty,
                })
            },
        )
    } else {
        Err(to_err_string(resp))
    }
}

fn parse_version_response(resp: PlotterResponse) -> Result {
    if resp.len() == 1 {
        VERSION_RE.captures(&resp[0]).map_or(
            Err(String::from("Could't parse EBB Response")),
            |cap| {
                let major = match_to_u8(cap.get(1));
                let minor = match_to_u8(cap.get(2));
                let patch = match_to_u8(cap.get(3));

                Ok(EBBResponse::Version {
                    major,
                    minor,
                    patch,
                })
            },
        )
    } else {
        Err(to_err_string(resp))
    }
}

pub fn toggle_pen() -> String {
    String::from("TP\r")
}

pub fn query_steps() -> String {
    String::from("QS\r")
}

#[derive(Debug, PartialEq, Eq)]
pub struct Steps {
    pub x_pos: i32,
    pub y_pos: i32,
}

pub fn parse_query_steps_response(resp: PlotterResponse) -> Result {
    match resp.len() {
        2 => {
            let maybe_steps: Vec<&str> = resp[0].split(',').collect();
            let x_pos: i32 = maybe_steps[0].parse().expect("Failed to parse");
            let y_pos: i32 = maybe_steps[1].parse().expect("Failed to parse");
            Ok(EBBResponse::Steps { x_pos, y_pos })
        }
        _ => Err(to_err_string(resp)),
    }
}

pub fn query_nickname() -> String {
    String::from("QT\r")
}

pub fn parse_query_nickname_response(resp: PlotterResponse) -> Result {
    match resp.len() {
        2 => {
            if !resp[0].is_empty() && resp[1] == "Ok" {
                Ok(EBBResponse::Nickname {
                    nickname: resp[0].to_string(),
                })
            } else {
                Ok(EBBResponse::Nickname {
                    nickname: "No nickname set".to_string(),
                })
            }
        }
        _ => Err(to_err_string(resp)),
    }
}

pub fn query_pen() -> String {
    String::from("QP\r")
}

pub fn parse_query_pen_response(resp: PlotterResponse) -> Result {
    match resp.len() {
        2 => {
            let maybe_pos = &resp[0];
            PenPosition::try_from(maybe_pos.as_ref())
                .and_then(|position| Ok(EBBResponse::PenPosition(position)))
        }
        _ => Err(to_err_string(resp)),
    }
}

/// Technically, the "Home Move" instruction is parameterized by an unsigned
/// integer between 2 and 25k, representing the steps-per-second speed at which
/// home should be returned to. This seems... very silly to me, so far; will
/// consider adding a parameter in the future.
///
/// Update: it's actually not silly; rather, it ties in to the whole
/// acceleration system I need to build.
// TODO[gastove|2021-01-02] This needs to do firmware detection, and use motor position if version < 2.6.2
pub fn home_move() -> String {
    String::from("HM,1000\r")
}

pub fn set_nickname(set_to: String) -> String {
    format!("ST,{}\r", set_to)
}

pub fn set_pen(set_to: &PenPosition) -> String {
    match set_to {
        PenPosition::Up => String::from("SP,1\r"),
        PenPosition::Down => String::from("SP,0\r"),
    }
}

#[cfg(test)]
mod instructions_test {

    use super::*;

    #[test]
    pub fn test_parse_query_steps_response() {
        let test_response = vec![String::from("1,2"), String::from("OK")];

        let expected = EBBResponse::Steps { x_pos: 1, y_pos: 2 };

        let got = parse_query_steps_response(test_response);

        assert_eq!(Ok(expected), got);
    }

    #[test]
    pub fn test_version_parse_result() {
        let test_str = String::from("EBBv13_and_above EB Firmware Version 2.4.2");

        let expected = EBBResponse::Version {
            major: 2,
            minor: 4,
            patch: 2,
        };

        let result = parse_version_response(vec![test_str]);

        assert_eq!(Ok(expected), result);
    }
}
