use std::{fs, os::unix::net::UnixListener, path::Path, thread};

use log::{debug, error, info};

use crate::api;

pub fn listen_and_handle(socket: &str, reference: bool) {
    if Path::new(socket).exists() {
        debug!("Removing {}", socket);
        fs::remove_file(socket)
            .expect("Socket {} is in use and could not be cleared, remove it and try again.");
    }

    let listener = UnixListener::bind(socket).unwrap();
    info!("Listening on {}", socket);

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                /* connection succeeded */
                info!("Connecting to client");
                thread::spawn(api::make_api_handler(stream, reference));
            }
            Err(err) => {
                error!("Connection failed: {}", err);
                break;
            }
        }
    }
    // TODO: need an interrupt handler somewhere in here to clean up the socket.
}
