#[macro_use]
extern crate clap;
extern crate draw_bridge;

use clap::{App, Arg};
use log::info;
use simplelog::{Config, LevelFilter, TermLogger};

use draw_bridge::socket;

mod cli {
    pub mod args {
        pub mod socket {
            pub const ARG_NAME: &str = "socket";
            pub const ENV_VAR: &str = "DRAWBRIDGE_SOCK";
            pub const HELP: &str = "The IPC socket drawbridge will listen on";
        }

        pub mod verbosity {
            pub const ARG_NAME: &str = "verbosity";
            pub const HELP: &str = "Set log verbosity; provide multiple times to dial it up";
        }

        pub mod reference {
            pub const ARG_NAME: &str = "reference";
            pub const HELP: &str =
                "Run the application in reference mode, which mocks all responses";
        }
    }
}

fn main() {
    let app = App::new("drawbridge")
        .about("An IPC socket-based API around EBB drawing robot devices")
        .author("Ross Donaldson")
        // This macro uses the version listed in the Cargo.toml -- ideal
        .version(&crate_version!()[..])
        .arg(
            Arg::with_name(cli::args::socket::ARG_NAME)
                .long(cli::args::socket::ARG_NAME)
                .short("s")
                .env(cli::args::socket::ENV_VAR)
                .help(cli::args::socket::HELP)
                .required(true),
        )
        .arg(
            Arg::with_name(cli::args::verbosity::ARG_NAME)
                .long(cli::args::verbosity::ARG_NAME)
                .short("v")
                .help(cli::args::verbosity::HELP)
                .multiple(true),
        )
        .arg(
            Arg::with_name(cli::args::reference::ARG_NAME)
                .long(cli::args::reference::ARG_NAME)
                .short("r")
                .help(cli::args::reference::HELP),
        );

    let matches = app.get_matches();

    let sock = matches.value_of(cli::args::socket::ARG_NAME).unwrap();

    // TODO[gastove|2020-12-27] Figure out setting verbosity from an env var as
    // an alternative.
    let log_level = match matches.occurrences_of(cli::args::verbosity::ARG_NAME) {
        0 => LevelFilter::Info,
        1 => LevelFilter::Debug,
        2 | _ => LevelFilter::Trace,
    };

    let reference = matches.is_present(cli::args::reference::ARG_NAME);

    TermLogger::init(log_level, Config::default()).unwrap();

    info!("Bridge coming online! Setting up...");
    info!("Opening socket and listening at {}", sock);
    socket::listen_and_handle(sock, reference);
}
