ARG SPECTAG=latest
ARG CTNSHA=latest

FROM registry.gitlab.com/draw-bridge/draw-bridge-site/api-spec:${SPECTAG} AS spec

FROM registry.gitlab.com/draw-bridge/draw-bridge/base:${CTNSHA}

COPY --from=spec /json /json
