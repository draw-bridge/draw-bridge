extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn;

#[proc_macro_derive(FromParams)]
pub fn from_params_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_from_params(&ast)
}

fn impl_from_params(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {

        impl FromParams for #name {
            type Item = #name;

            fn from_params(params: &[String]) -> Vec<Self::Item> {
                params
                    .iter()
                    .filter_map(|param| serde_json::from_str::<Self::Item>(&param).ok())
                    .collect()
            }
        }
    };
    gen.into()
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
